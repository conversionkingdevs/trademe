//wait for selector then run method
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function start(){
   //adds font-awesome
    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='screen'>");    
    jQuery("body").addClass("opt-mba");
    addMenuNevigationSection();
    jQuery('#section-menu-nav li').hover(function(){
                var vb = '#sub-content'+jQuery(this).attr('id');
                jQuery('body').find(vb).toggleClass('expand');
   });

    jQuery('#sub-contentls_5').remove();
    jQuery('#sub-contentls_2 ul').each(function () {
        jQuery(this).find('li:eq(0)').after('<div class="optUlContainer"></div>');
        jQuery(this).find('li:not(:nth-child(1))').appendTo(jQuery(this).closest('ul').find('.optUlContainer'));
    });

    jQuery('#sub-contentls_2 ul > li:nth-child(1)').hover(function () {
        jQuery('.optHovered').removeClass('optHovered');
        jQuery(this).closest('ul').addClass('optHovered');
    });
    jQuery('#sub-contentls_2 ul:eq(0)').addClass('optHovered');

    jQuery('.sub-img > button').click(function () {
        window.location = jQuery(this).find('a').attr('href');
    });

}
var menu_ls = {
        "menu":[{
                    "title":'Home & Living',
                    "img": '<img src="//cdn.optimizely.com/img/2371960453/ea884d56164347b888c3339f29a6cb68.png" alt="Home & Living">',
                    "sub_title": '<span>HOME & LIVING</span><button><a href="https://www.trademe.co.nz/home-living" style="color: white;">View all</a></button>',
                    "btn":'<button>View all</button>',
                    "list": [
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/bathroom">Bathroom <span>(4865)</span> </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/bedding-towels">Bedding &amp; towels <span>(19455)</span> </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/bedroom-furniture">Bedroom furniture <span>(8292)</span> </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/beds">Beds <span>(6956) </span> </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/cleaning-bins">Cleaning &amp; bins <span>(5442)</span>  </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/curtains-blinds">Curtains &amp; blinds <span>(6036)</span>  </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/food-beverage">Food &amp; beverage <span>(6195)</span> </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/heating-cooling">Heating &amp; cooling <span>(3835)</span>  </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/home-d%C3%A9cor">Home décor <span>(78893)</span>  </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/kitchen">Kitchen <span>(79009)</span>  </a>'
                            ],
                    "list2": [
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/lamps">Lamps (7829) </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/laundry">Laundry (3996)</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/lifestyle">Lifestyle (5975) </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/lounge-dining-hall">Lounge, dining &amp; hall (35701)</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/luggage-travel-accessories">Luggage &amp; travel accessories (6753) </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/outdoor-garden-conservatory">Outdoor, garden &amp; conservatory (52059)</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/party-festive-supplies">Party &amp; festive supplies (51886) </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/security-locks-alarms">Security, locks &amp; alarms (5913) </a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/home-living/wine">Wine (1363)</a>'
                    ],
                    "list3": [],
                    "list4": [],
                } ,
                {
                    "title":'Sports',
                    "img": ' ',
                    "sub_title": ' ',
                    "btn":' ',
                    "list": [
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/aviation">Aviation <span>(189) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/basketball">Basketball <span>(1260) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/bowls-bowling">Bowls & bowling <span>(307) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/camping-outdoors">Camping & outdoors <span>(38560) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/cricket">Cricket <span>(2579)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/cycling">Cycling <span>(49403) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/dancing-gymnastics">Dancing & gymnastics <span>(2354) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/darts">Darts <span>(1772) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/equestrian">Equestrian <span>(6393) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/exercise-equipment-weights">Exercise equipment & weights <span>(10373) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/fishing">Fishing <span>(20293) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/golf">Golf <span>(5841) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/gym-memberships">Gym memberships <span>(28) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/hockey">Hockey <span>(1384)</span></a>',
                        ],
                    "list2": [
                        
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/hunting-shooting">Hunting & shooting <span>(23350)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/kayaks-canoes">Kayaks & canoes <span>(1715)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/kites-kitesurfing">Kites & kitesurfing <span>(278) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/martial-arts-boxing">Martial arts & boxing <span>(4554)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/netball">Netball <span>(533)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/paintball">Paintball <span>(1290)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/protective-gear-armour">Protective gear & armour <span>(745)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/racquet-sports">Racquet sports <span>(1982)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/rugby-league">Rugby & league <span>(11748)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/running-track-field">Running, track & field <span>(3361)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/scuba-snorkelling">SCUBA & snorkelling <span>(4063)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/skateboarding-rollerblading">Skateboarding & rollerblading <span>(9921)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/ski-board">Ski & board <span>(10295)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/snooker-pool">Snooker & pool <span>(586)</span></a>',
                    ],
                    "list3": [
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/soccer">Soccer <span>(9828)</span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/softball-baseball">Softball & baseball <span>(215) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/sports-bags">Sports bags <span>(1051) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/sports-memorabilia">Sports memorabilia <span>(2938) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/sports-nutrition-supplements">Sports nutrition & supplements <span>(3384) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/surfing">Surfing <span>(3574) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/swimming">Swimming <span>(2052) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/trading-cards">Trading cards <span>(1018) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/waterskiing-wakeboarding">Waterskiing & wakeboarding <span>(760) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/wetsuits-lifejackets">Wetsuits & lifejackets <span>(3957) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/windsurfing">Windsurfing <span>(99) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/sports/other">Other <span>(6165) </span></a>',
                        '<a target="_blank" href="https://www.trademe.co.nz/motors/boats-marine">Related categories: Boats & marine</span></a>',
                    ],
                    "list4": [],
                } ,
                {                  
                    "title":"Clothing & Fashion",
                    "img": "",
                    "sub_title": "",
                    "btn": "",                    
                    "list": [
                            "<a href='https://www.trademe.co.nz/clothing-fashion/women' class='optBlackLink'>Women</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/tops-shirts'>Tops & Shirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/dresses'>Dresses</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/jeans-pants-shorts'>Jeans, pants, & shorts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/skirts'>Skirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/jackets'>Jackets</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/suits'>Suits</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/bulk-lots'>Bulk Lots</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/shoes'>Shoes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/accessories'>Accessories</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/bags-handbags>Bags & Handbags </a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/wedding-accessories'>Wedding accessories</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/lingerie-sleepwear'>Lingerie & Sleepwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/swimwear'>Swimwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/sportswear'>Sportswear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/maternity'>Maternity</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/vintage-retro'>Vintage & Retro</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/wedding'>Wedding</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/costumes'>Costumes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/novelty-adult'>Novelty & adult</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/women/other'>Other</a>"
                        ],
                    "list2": [
                            "<a href='https://www.trademe.co.nz/clothing-fashion/men' class='optBlackLink'>Mens</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/tops-shirts'>Tops & Shirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/tshirts'>T-shirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/jerseys-hoodies'>Jerseys & hoodies</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/jeans-trousers'>Jeans & Trousers</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/shorts'>Shorts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/jackets'>Jackets</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/bulk-lots'>Bulk Lots</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/shoes'>Shoes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/accessories'>Accessories</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/underwear'>Underwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/socks'>Socks</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/sportswear'>Sportswear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/suits'>Suits</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/costumes'>Costumes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/wedding'>Wedding</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/novelty-adult'>Novelty & Adult</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/men/other'>Other</a>"
                        ],
                    "list3": [
                            "<a href='https://www.trademe.co.nz/clothing-fashion/girls' class='optBlackLink'>Girls</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/dresses-skirts'>Dresses & Skirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/tops-tshirts'> Tops & t-shirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/jeans-trousers'>Jeans & Trousers</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/jackets'>Jackets</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/shorts'>Shorts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/jerseys-hoodies'>Jerseys & hoodies</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/bulk-lots'>Bulk Lots</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/shoes'>Shoes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/bags-accessories'>Bags & accessories</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/pyjamas'>Pyjamas</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/swimwear'>Swimwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/underwear'>Underwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/socks'>Socks</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/uniforms'>Uniforms</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/costumes'>Costumes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/wedding'>Wedding</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/girls/other'>Other</a>"
                        ],
                    "list4": [
                            "<a href='https://www.trademe.co.nz/clothing-fashion/boys' class='optBlackLink'>Boys</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/tops-tshirts'> Tops & t-shirts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/jerseys-hoodies'>Jerseys & hoodies</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/jeans-trousers'>Jeans & trousers</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/shorts'>Shorts</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/jackets'>Jackets</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/bulk-lots'>Bulk Lots</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/shoes'>Shoes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/bags-accessories'>Bags & accessories</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/pyjamas'>Pyjamas</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/socks-underwear'>Socks & underwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/swimwear'>Swimwear</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/uniforms'>Uniforms</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/costumes'>Costumes</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/wedding'>Wedding</a>",
                            "<a target='_blank' href='http://www.trademe.co.nz/clothing-fashion/boys/other'>Other</a>"
                        ],
                },
                {
                    "title":'Electronics & Photography',
                    "img": ' ',
                    "sub_title": ' ',
                    "btn":' ',
                    "list": [
                            '<a href="" class="optOrange">Home audio & Video</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/dvd-bluray-players">DVD & Blu-ray players<span> (630) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/home-audio">Home audio<span> (7638) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/projectors-screens">Projectors & screens<span> (1087) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/tvs">TVs<span> (8767) </span></a>',
                            '<a href="" class="optOrange">Personal audio & iPods</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/ipod-mp3-accessories">iPod & MP3 accessories<span> (5165)</span></a>',
                            '<a href="" class="optOrange">Media & consumables</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/batteries">Batteries<span> (3989)</span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/memory-cards">Memory cards<span> (1598) </span></a>',
                        ],
                        "list2": [
                            '<a href="" class="optOrange">Photography</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/camera-accessories">Camera accessories<span> (16455)</span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/digital-cameras">Digital cameras<span> (1289) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/film-cameras">Film cameras<span> (933) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/video-cameras">Video cameras<span> (4295) </span></a>',
                            '<a href="" class="optOrange">Other electronics</a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/binoculars-telescopes">Binoculars & telescopes<span> (718)</span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/gps">GPS<span> (873) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/phone-fax">Phone & fax<span> (1696) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/radio-equipment">Radio equipment<span> (1084) </span></a>',
                            '<a target="_blank" href="https://www.trademe.co.nz/electronics-photography/other-electronics">Other electronics<span> (8613) </span></a>',
                        ],
                        "list3": [],
                        "list4": [],
                },
                {
                    "title":'Computers',
                    "img": '<img src="//cdn.optimizely.com/img/2371960453/a2f981dba40e4ce9ab75b665dbcefd3c.jpg">',
                    "sub_title": '<span>Computers</span><button><a href="https://www.trademe.co.nz/computers" style="color: white;">View all</a></button>',
                    "btn":'<button>View all</button>',
                    "list": [
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/blank-discs-cases'>Blank discs & cases <span>(438) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/cables-adaptors'>Cables & adaptors <span>(8971)</span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/components'>Components <span>(8082) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/computer-furniture'>Computer Furniture <span>(943) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/desktops>Desktops <span>(3557) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/domain-names'>Domain names <span>(34) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/external-storage>External Storage <span>(1587) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/laptops>Laptops <span>(28009) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/monitors'>Monitors <span>(961) </span></a>",
                        ],
                    "list2": [
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/networking-modems'>Networking & modems <span>(4976) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/peripherals'>Peripherals <span>(6888) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/printer-accessories-supplies'>Printer accessories & supplies <span>(10092) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/printers'>Printers <span>(1380) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/servers'>Servers <span>(1099) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/software'>Software <span>(561) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/tablets-ebook-readers'>Tablets & E-book readers <span>(11940) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/vintage'>Vintage <span>(314) </span></a>",
                        "<a target='_blank' href='https://www.trademe.co.nz/computers/other'>Other <span>(1147) </span></a>",
                    ],
                    "list3": [],
                    "list4": [],
                },
                
                {
                    "title":'<a class="optNoClick" href="https://www.trademe.co.nz/marketplace">Marketplace</a>',
                    "img": ' ',
                    "sub_title": ' ',
                    "btn":' ',                    
                    "list": [],
                    "list2": [],
                    "list3": [],
                    "list4": [],
                },
                {
                    "title":'View All Categories <i class="fa fa-angle-right" aria-hidden="true"></i>',
                    "img": ' ',
                    "sub_title": ' ',
                    "btn":' ',                    
                    "list": [
                        '<a target="_blank" href="http://www.trademe.co.nz/antiques-collectables">Antiques & collectables</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/art">Art</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/baby-gear">Baby gear</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/books">Books</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/building-renovation">Building & renovation</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/business-farming-industry">Business & industry</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/motors">Cars, bikes & boats</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/Link.aspx?i=73305">Clothing & fashion</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/computers">Computers</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/crafts">Crafts</a>',
                    ],
                    "list2": [
                        '<a target="_blank" href="http://www.trademe.co.nz/electronics-photography">Electronics & photography</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/business-farming-industry/farming-forestry">Farming</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/flatmates-wanted">Flatmates wanted</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/gaming">Gaming</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/health-beauty">Health & beauty</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/home-living">Home & living</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/jewellery-watches">Jewellery & watches</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/jobs">Jobs</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/mobile-phones">Mobile phones</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/Movies-TV/index.htm">Movies & TV</a>',
                    ],
                    "list3": [
                        '<a target="_blank" href="http://www.trademe.co.nz/music-instruments">Music & instruments</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/pets-animals">Pets & animals</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/pottery-glass">Pottery & glass</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/property">Real estate</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/services">Services</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/sports">Sports</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/toys-models">Toys & models</a>',
                        '<a target="_blank" href="http://www.trademe.co.nz/travel-events-activities">Travel, events & activities</a>',
                    ],
                    "list4": [],
                }             
            ]
};
function addMenuNevigationSection(){
    var codehtml = "";
    codehtml = codehtml+"<div id='section-menu-nav' class='menu-nav'>"+
                            "<ul>";
    for(i=0; i< menu_ls['menu'].length; i++){
        codehtml = codehtml + "<li class='lsit' id='ls_"+i+"'> "+ menu_ls['menu'][i]['title']+"</il>";
    }
    
    codehtml = codehtml +  "</ul>";

    for(i=0; i< menu_ls['menu'].length; i++){
        codehtml = codehtml +  "<div id='sub-contentls_"+i+"' class='sub-content'>"+
                                "<ul>";
       
            for(j=0; j< menu_ls['menu'][i]['list'].length; j++){
                codehtml = codehtml + "<li>"+menu_ls['menu'][i]['list'][j]+"</li>";
            }
       
        codehtml = codehtml +  "</ul>";
        codehtml = codehtml + "<ul>";

            for(k=0; k< menu_ls['menu'][i]['list2'].length; k++){
                codehtml = codehtml + "<li>"+menu_ls['menu'][i]['list2'][k]+"</li>";
            }

            codehtml = codehtml +  "</ul>";
            codehtml = codehtml + "<ul>";

            for(m=0; m< menu_ls['menu'][i]['list3'].length; m++){
                codehtml = codehtml + "<li>"+menu_ls['menu'][i]['list3'][m]+"</li>";
            }
            
            codehtml = codehtml +  "</ul>";
            codehtml = codehtml + "<ul>";

            for(n=0; n< menu_ls['menu'][i]['list4'].length; n++){
                codehtml = codehtml + "<li>"+menu_ls['menu'][i]['list4'][n]+"</li>";
            }

        codehtml = codehtml + "</ul>";
        codehtml = codehtml + "<div class='sub-img' >";
        codehtml = codehtml +  menu_ls['menu'][i]['img'];
        codehtml = codehtml + menu_ls['menu'][i]['sub_title'];
        codehtml = codehtml +  "</div>";
        codehtml = codehtml +  "</div>";

    }

 
    codehtml = codehtml+"</div>";
    jQuery("#SiteHeader_SiteTabs_BarOfSearch_SearchBarDiv").after(codehtml);
}
    

defer(function(){
    start();
}, "#top-of-page");


