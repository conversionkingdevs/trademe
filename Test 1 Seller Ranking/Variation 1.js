// Create & add the review tile
function addReview (selector) {
    var url = jQuery('.seller-profile-member-rating-container a:eq(0)').attr('href'),
        name = jQuery('#SellerProfile_MemberNicknameLink').text(),
        starsText = jQuery('.seller-profile-member-rating-container').text().trim(),
        starsImg = jQuery('#SellerProfile_memberRatingStars').attr('class'),
        rating = jQuery('#SellerProfile_PercentFeedbackMessage span').text().trim().replace("%","% ");

    jQuery(selector).prepend('<div class="buyer-protection optBuyerCont"> <div class="optLeftCont"> <a href="'+url+'"> <p class="optBlue">'+name+'<span class="optBlack"> ('+starsText+'</span><span class="'+starsImg+'"></span><span class="optBlack">)</span></p><p class="optGrey">'+rating+'</p></a> </div><div class="optRightCont"> </div></div>');
}

// Logic needs to be confirmed for edge cases
/*if (jQuery(".sidebar-content #Bidding_bidAuctionContent").length >= 1) { 
    addReview("#Bidding_bidAuctionContent");
} else if (jQuery(".buy-now-box-container").length >= 1) {
    addReview(".buy-now-box-container");
}*/
addReview('.sidebar-content');