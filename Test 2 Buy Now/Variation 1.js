

// Functions
    // Check elements exist
    function checkForelements(arr) { // Accepts an array of specific selectors to check
        var check = true;
        for (var i = 0; i < arr.length; i++) {
            if (jQuery(arr[i]).length > 0 && jQuery(arr[i]).css('display') != "none") {
                check = true;
            } else {
                check = false;
            }

        }
        return check;
    }

    // Check values are the same
    function checkSameValue (arr) { // Accepts an array of specific selectors to check values against
        
        var valueArr = [], // Holds array of values
            check = false;

        for (var i = 0; i < arr.length; i++) { // Makes the array of values
            var nodetype = jQuery(arr[i]).prop('nodeName');
            switch (nodetype) {
                case "DIV":
                    var val = jQuery(arr[i]).text();
                    if (val.indexOf('.') == -1) {
                        val = val+'.00'                    
                    }
                     valueArr.push(val);
                    break;
                case "INPUT":
                    var val = jQuery(arr[i]).val();
                    if (val.indexOf('.') == -1) {
                        val = val+'.00'                    
                    }
                     valueArr.push(val);
                    break;
            }
        }

        for (var i = 0; i < valueArr.length; i++) {
            for (var l = i + 1; l < valueArr.length; l++) {
                if (valueArr[i] == valueArr[l]) {
                    check = true;
                } else if (valueArr[i] != valueArr[l]) {
                    return false;
                }
            }
        }
         return check;
    }

if (checkForelements(["#Watchlist_SaveToWatchlistButton", "#addToCartButton","#quickBuyNowButton"]) && checkSameValue(["#BuyNow_BuyNow", "#Bidding_CurrentBidValue", "#Bidding_bidFormBid"])) {
    jQuery('#aspnetForm, .buynow-title, #addToCartButton, .bid-details').hide();
    jQuery('#Bidding_ShippingInfo').addClass('optMarginFix');
    jQuery('body').addClass('opt2'); // Body Class for tracking & css
}