jQuery('body').addClass('opt3'); // Body Class for tracking & css
jQuery('head').append('<script src="https://use.fontawesome.com/47642bd449.js"></script>'); // Add Font-awesome for fa-eye icon - Consider uploading image 

if (jQuery('#quickBuyNowButton').length >= 0 && jQuery('#quickBuyNowButton').css('display') != "none") { // Check if there is a Buy Now button
    jQuery('#Watchlist_SaveToWatchlistButton').detach().insertAfter('.buynow-content form'); // Move the Watchlist
    jQuery('#Watchlist_SaveToWatchlistButton').prepend('<span class="fa fa-eye"></span>'); // Add the fa-eye icon

}