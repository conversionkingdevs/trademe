// Create & add the review tile
function addReview (selector) {
    var newRating = jQuery('.feedback-column-flag:eq(0)')[0].outerHTML,
        newName = jQuery('#SellerProfile_MemberText')[0].outerHTML;

        jQuery(selector).prepend('<div class="optHolder mp-listing"><div class="optLeft">'+newRating+'</div><div class="optRight">'+newName+'</div></div>')
}
addReview('.sidebar-content');